﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SimpleWebSiteLibrary;

namespace SimpleWebSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            CustomPageCollection pages = new CustomPageCollection();
            pages.Fill(pages.GetActivityList(true));
            foreach (CustomPage cpage in pages)
            {
                routes.MapRoute(
                name: "CustomPage" + cpage.id.ToString(),
                url: cpage.urlPath,
                defaults: new { controller = "CustomPage", action = "Index", id = cpage.id },
                namespaces: new string[] { "SimpleWebSite.Controllers" }
            );
            }

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = SimpleWebSiteLibrary.SystemSettings.DefaultController, action = SimpleWebSiteLibrary.SystemSettings.DefaultAction, id = UrlParameter.Optional },
                namespaces: new string[] { "SimpleWebSite.Controllers" }
            );
        }
    }
}