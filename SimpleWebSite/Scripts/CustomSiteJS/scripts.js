﻿/*Login Page*/
function LoginUser(returnUrl) {
    $.post("/login/LoginUser",
        {
            "login": $("#txtLogin").val(),
            "password": $("#txtPassword").val(),
            "remember": $("#chRememberMe").is(":checked")
        },
        function (data) {
            var obj = JSON.parse(data);
            
            if (obj[0] == "true") {
                window.location.replace((returnUrl == "") ? "/" : returnUrl);
            }
            else {
                bootbox.alert(obj[1]);
            }
        });
}

function SendEmailConfirmation(userLogin) {
    $.post("/login/SendEmailConfirmation",
        {
            "userLogin": userLogin
        },
        function (data) {
            var obj = JSON.parse(data);            
            bootbox.alert(obj[1],
                function () {
                    bootbox.hideAll();
                });
        });
}

/*Register Page*/
function ShowRules() {
    event.preventDefault();
    var modalForm = $("<div class='modal fade'></div>");
    $.post("/register/ShowRegisterRules",
        function (data) {
            $(modalForm).html(data);
            $(modalForm).modal("show");
        });
}

function RegisterUser() {
    $.post("/register/Register",
        {
            "txtLogin": $("#txtLogin").val(),
            "txtEmail": $("#txtEmail").val(),
            "txtPassword": $("#txtPassword").val(),
            "txtPassword2": $("#txtPassword2").val(),
            "CaptureImageText_txtCapcha": $("#CaptureImageText_txtCapcha").val(),
            "CheckRules": $("#CheckRules").is(":checked")
        },
        function (data) {
            var obj = JSON.parse(data);
            bootbox.alert(obj[1],
                function () {
                    if (obj[0] == "True" || obj[0] == "true") {
                        window.location.replace("/");
                    }
                });
        });
}

function CheckUserEmailExists(input) {
    var flag = true;
    $.ajaxSetup({ async: false });
    $.post("/register/CheckEmailExists",
        {
            "email": $(input).val()
        },
        function (data) {
            var obj = JSON.parse(data);
            if (obj[0] == "true") {
                $(input).attr("data-placement", "top");
                $(input).attr("data-toggle", "tooltip");
                $(input).attr("data-content", obj[1]);
                $(input).parent().parent().addClass("has-error");
                $(input).popover("show");
                flag = true;
            }
            else {
                $(input).popover('destroy');
                $(input).parent().parent().removeClass("has-error");
                flag = false;
            }
        });
    $.ajaxSetup({ async: true });

    return flag;
}

function RegisterSocialUserWithEmail(input, socialType) {    
    event.preventDefault();
    if (!CheckUserEmailExists($(input))) {
        $.post("/login/AuthorizeSocialsWithEmail",
            {
                "SocialType": socialType,
                "Email": $(input).val()
            },
            function (data) {
                var obj = JSON.parse(data);
                if (obj[0] == "false") {                    
                    bootbox.alert(obj[1]);
                }
                else {
                    window.location.replace("/");
                }
            });
    }
}

/*End Of Register Page*/