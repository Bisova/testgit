﻿$(function () {
    bootbox.setDefaults({
        locale: "ru"
    });

    $.extend($.fancybox.defaults.tpl, {
            error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Файл изображения недоступен. Попробуйте повторить попытку позже.</p>',
            closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
            next: '<a title="Следующее" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
            prev: '<a title="Предыдущее" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
        }
    );

    $("a.fancybox").fancybox();
});
/*Проверка валидности адреса электронной почты*/
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
/*Множественная форма слова*/
function PluralFrom(langId, n, WordForms) {
    plural = 0;
    switch (langId) {
        case 0:
            plural = (n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
            break;
        case 1:
            plural = parseInt((n != 1));
            break;
        case 2:
            plural = parseInt((n != 1));
            break;
        default:
            return WordForms[0];
    }

    return WordForms[plural];
}

/*Получить текст сообщения по заданному коду*/
function InfoMessage(key) {
    var mess = "";
    $.ajaxSetup({ async: false });
    $.post("/system/Message",
        {
            "id": key
        },
        function (data) {
            mess = data;
        });
    $.ajaxSetup({ async: true });
    return mess;
}