﻿//Ололошиньки-ло-ло-ло-ло-ло


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomSiteMapLibrary;
using SimpleWebSite.ViewModels;

namespace SimpleWebSite.Controllers
{
    public class MainController : Controller
    {
        /// <summary>
        /// Модель предстваления главного контроллера 567874867676758757575745475425424242545
        /// </summary>
        public MainControllerViewModel model
        {
            get;
            set;
        }

        /// <summary>
        /// Главный контроллер
        /// </summary>
        public MainController()
        {
            model = new MainControllerViewModel();
            ViewBag.Model = model;
            
            sqlSiteMapProvider myProvider = (sqlSiteMapProvider)SiteMap.Provider;
            myProvider.Refresh();
            ViewBag.IsConfirmed = model.IsUserConfirmed;
        }

    }
}
