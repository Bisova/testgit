﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleWebSiteLibrary;
using System.Web.Security;

namespace SimpleWebSite.Controllers
{
    public class LoginController : MainController
    {

        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (User.Identity.IsAuthenticated)
                return Redirect("/");
            return View(model);
        }

        [HttpPost]
        public string LoginUser(string login, string password, bool remember)
        {
            SimpleWebSiteLibrary.User us = new SimpleWebSiteLibrary.User();
            string message = string.Empty;
            bool flag =  us.ValidateUser(login, password, out message);
            if (flag)
            {
                FormsAuthentication.SetAuthCookie(us.login, remember);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(new string[]{flag.ToString().ToLower(), message});
        }



        #region<<Socials>>

        public ActionResult AuthorizeVK(string code, string returnUrl)
        {
            bool IsEmailExists = false;    
            string query =
                        "https://oauth.vk.com/access_token?" +
                        "client_id=" + Socials.Settings.VKapiId + "&" +
                        "client_secret=" + SimpleWebSiteLibrary.SystemSettings.VKSecretKey + "&" +
                        "code=" + code + "&" +
                        "redirect_uri=http://" + Request.Url.Host + "/login/AuthorizeVK?returnUrl=" + returnUrl;

                System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(query);
                System.Net.HttpWebResponse resp = (System.Net.HttpWebResponse)req.GetResponse();

                using (System.IO.StreamReader stream = new System.IO.StreamReader(
                    resp.GetResponseStream(), System.Text.Encoding.UTF8))
                {
                    Dictionary<string, string> options = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(stream.ReadToEnd());
                    Session["AuthToken"] = options["access_token"];
                    Session["UserEmail"] = options["email"];
                    Session["UserSocialId"] = options["user_id"];
                }

                string UserGetQuery = "https://api.vk.com/method/users.get?user_id=" + Session["UserSocialId"].ToString() + "&fields=photo_200,photo_50&v=5.24&access_token=" + Session["AuthToken"].ToString();

                req = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(UserGetQuery);
                resp = (System.Net.HttpWebResponse)req.GetResponse();

                string firstName = "";
                string lastName = "";
                string avatar = "";
                string avatarsmall = "";
                    
                using (System.IO.StreamReader stream = new System.IO.StreamReader(
                    resp.GetResponseStream(), System.Text.Encoding.UTF8))
                {
                    string sq = stream.ReadToEnd();
                    sq = sq.Replace("{\"response\":[", string.Empty).Replace("}]}", "}");
                    Dictionary<string, string> options =
                        Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(sq);

                    firstName = options["first_name"];
                    lastName = options["last_name"];
                    avatar = options["photo_200"];
                    avatarsmall = options["photo_50"];

                    Session["UserFirstName"] = firstName;
                    Session["UserLastName"] = lastName;
                    Session["UserAvatar"] = avatar;
                    Session["UserAvatarSmall"] = avatarsmall;
                }
                try
                {

                string message = "";
                SimpleWebSiteLibrary.User us = new User();
                bool flag = us.ValidateUserSocials("vk", "vk_" + Session["UserSocialId"].ToString(), 
                    Session["UserEmail"].ToString(), firstName, lastName, avatar, avatarsmall, out message, out IsEmailExists);
                if (IsEmailExists)
                    return Redirect("/login/emailexists?socialType=vk&email=" + Session["UserEmail"].ToString());
                if (flag)
                {
                    FormsAuthentication.SetAuthCookie("vk_" + Session["UserSocialId"].ToString(), true);
                }
            }
            catch (Exception ex)
            {
                errors.error_messageEx = ex;
            }
            return Redirect((string.IsNullOrEmpty(returnUrl)?"/":returnUrl));
        }
        
        [HttpPost]
        public string AuthorizeSocialsWithEmail(string SocialType, string Email)
        {
            if (!SystemSettings.AllowSocialsAuthorization || Session["AuthToken"] == null ||
                string.IsNullOrEmpty(SocialType) || string.IsNullOrEmpty(Email))
                return Newtonsoft.Json.JsonConvert.SerializeObject(new string[] { "false", sql.Message("LoginSocialDeny") });

            SimpleWebSiteLibrary.User us = new User();
            string message = "";
            bool IsEmailExists = false;
            bool flag = us.ValidateUserSocials(SocialType, SocialType+"_" + Session["UserSocialId"].ToString(),
                Email, Session["UserFirstName"].ToString(), Session["UserLastName"].ToString(), Session["UserAvatar"].ToString(), Session["UserAvatarSmall"].ToString(), out message, out IsEmailExists);
            if (flag)
            {
                FormsAuthentication.SetAuthCookie("vk_" + Session["UserSocialId"].ToString(), true);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(new string[] { flag.ToString().ToLower(),message });
        }

        public ActionResult EmailExists(string socialType, string Email)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("/");
            if (!SystemSettings.AllowSocialsAuthorization || Session == null || Session["AuthToken"] == null ||
                string.IsNullOrEmpty(socialType) || string.IsNullOrEmpty(Email))
                throw new HttpException(404, sql.Message("Error404"));

            ViewBag.Email = Email;
            ViewBag.SocialType = socialType;
            return View(model);
        }

        public ActionResult RegisterSocialCancelEmaiConfirmation()
        {
            Session.Clear();
            return Redirect("/login");
        }
        
        #endregion

        [Authorize]
        [HttpPost]
        public ActionResult LogOutUser(string ReturnUrl)
        {
            ReturnUrl = (string.IsNullOrEmpty(ReturnUrl)) ? "/" : ReturnUrl;
            User user = new User();
            if (user.LogOut(User.Identity.Name))
            {
                FormsAuthentication.SignOut();
            }

            return Redirect(ReturnUrl);
        }
        [HttpPost]
        public string SendEmailConfirmation(string userLogin)
        {
           string message = string.Empty;
           bool flag = SimpleWebSiteLibrary.User.SendEmailConfirmation(userLogin, out message);

           return Newtonsoft.Json.JsonConvert.SerializeObject(new string[] { flag.ToString().ToLower(), message });
        }

        public ActionResult ConfirmAccount(string id)
        {
            string message;
            ViewBag.flag = SimpleWebSiteLibrary.User.ConfirmAccount(id, out message);
            ViewBag.message = message;

            return View(model);
        }
    }
}
