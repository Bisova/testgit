﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleWebSite.Controllers
{
    public class HomeController : MainController
    {
        [Authorize]
        public ActionResult Index()
        {
            return View(model);
        }

    }
}
