﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleWebSite.Controllers
{
    public class SystemController : Controller
    {
        //
        // GET: /System/
        [HttpPost]
        public string Message(string id)
        {
            return SimpleWebSiteLibrary.sql.Message(id);
        }

    }
}
