﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleWebSiteLibrary;

namespace SimpleWebSite.Controllers
{
    public class RegisterController : MainController
    {
        //
        // GET: /Register/

        public ActionResult Index()
        {
            if (!SystemSettings.EnableRegister)
                throw new HttpException(404, "Запрашиваемая страница не найдена или была удалена.");
            if (User.Identity.IsAuthenticated)
                return Redirect("/");
            return View(model);
        }
        [HttpPost]
        public ActionResult ShowRegisterRules()
        {
            return PartialView("~/Views/Register/RegisterRules.cshtml");
        }
        [HttpPost]
        public string Register(string txtLogin, string txtEmail, string txtPassword, string txtPassword2,
            string CaptureImageText_txtCapcha, bool CheckRules)
        {
            bool flag = false;
            string message = string.Empty;

            SimpleWebSiteLibrary.User us = new User();
            flag = us.Register(txtEmail, txtLogin, txtPassword, txtPassword2, CheckRules, "txtCapcha",
                CaptureImageText_txtCapcha, out message);

            return Newtonsoft.Json.JsonConvert.SerializeObject(new string[] { flag.ToString(), message });
        }
        [HttpPost]
        public string CheckEmailExists(string email)
        {
            string message = "";
            bool flag = SimpleWebSiteLibrary.User.EmailExists(email, out message);
            return Newtonsoft.Json.JsonConvert.SerializeObject(new string[] { flag.ToString().ToLower(), message });
        }
    }
}
