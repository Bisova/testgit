﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleWebSiteLibrary;

namespace SimpleWebSite.Areas.Admin.Controllers
{
    public class NewsController : MainController
    {
        public ActionResult Index()
        {
            int CurrentPage = 1;
            try
            {
                CurrentPage = Convert.ToInt32(Request.QueryString["page"].ToString());
            }
            catch
            {
                return Redirect("/admin/news?page=1");
            }
            NewsCollection list = new NewsCollection();
            list.Fill(list.GetTypedList(1, CurrentPage, SystemSettings.AdminNewsOnPage));
            model.ChildViewModel = list;
            return View(model);
        }
        [HttpPost]
        public ActionResult ReloadNewsList(int page)
        {
            NewsCollection list = new NewsCollection();
            list.Fill(list.GetTypedList(1, page, SystemSettings.AdminNewsOnPage));
            return PartialView("~/Areas/Admin/Views/News/NewsList.cshtml", list);
        }
    }
}
