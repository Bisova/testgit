﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleWebSite.Areas.Admin.ViewModels;
using CustomSiteMapLibrary;

namespace SimpleWebSite.Areas.Admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class MainController : Controller
    {
        /// <summary>
        /// Модель предстваления главного контроллера
        /// </summary>
        public MainControllerViewModel model
        {
            get;
            set;
        }
        /// <summary>
        /// Главный контроллер
        /// </summary>
        public MainController()
        {
            model = new MainControllerViewModel();
            ViewBag.Model = model;

            sqlSiteMapProvider myProvider = (sqlSiteMapProvider)SiteMap.Provider;
            myProvider.Refresh();
        }
    }
}
