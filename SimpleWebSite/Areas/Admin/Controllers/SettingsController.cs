﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleWebSiteLibrary;

namespace SimpleWebSite.Areas.Admin.Controllers
{
    public class SettingsController : MainController
    {
        //
        // GET: /Admin/Settings/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public bool ClearCache()
        {
            return sql.ClearCache();
        }
    }
}
