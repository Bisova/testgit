﻿function LogOutUser() {
    bootbox.confirm("Выйти из системы?", function (result) {
        if (result) {
            $.ajaxSetup({ async: false });
            $.post("/login/LogOutUser",
                {
                    "ReturnUrl": "/"
                });
            $.ajaxSetup({ async: true });
            window.location.replace("/");
        }
    });
}

/*NewsBlock*/
function changeAdminNewsPage(obj) {
    event.preventDefault();
    var pageId = $.url(obj).param("page");
    $.post("/admin/news/ReloadNewsList",
        {
            "page": pageId
        },
        function (data) {
            $("#AdminNewsTableContainer").html(data);
        });
}
/*End News Block*/

/*Settings Block*/
function ClearCache() {
    event.preventDefault();
    bootbox.confirm("<b>Внимание!</b> Данная операцию может привести к временному замедлению работы приложения. Выполнить очистку кэша приложения?",
        function (result) {
            if (result) {
                $.post("/admin/settings/ClearCache",
                    function (data) {
                        if (data == "True") {
                            bootbox.alert("Кэш приложения был успешно очищен.");
                        }
                        else {
                            bootbox.alert("Не удалось выполнить операцию очистки кэша приложения из-за технической ошибки. Обратитесь к администратору приложения.");
                        }
                    });
            }
        });
}
/*End Of Settings Block*/