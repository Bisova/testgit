﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.Collections;
using System.Text.RegularExpressions;

namespace SimpleWebSite.Areas.Admin.Controls
{
    public static class TableControl
    {

        public static MvcHtmlString TableInput(this HtmlHelper html, string id, string tableCSS, string[] tableSchema, object ObjectsCollection, string pageUrl, string OnPageChageFunction)
        {
            int PageNumber = 0;
            int PageSize = 0;
            int TotalCount = 0;

            Type CollType = ObjectsCollection.GetType();
            try
            {
                PageNumber = Convert.ToInt32(CollType.GetProperty("PageNumber").GetValue(ObjectsCollection));
            }
            catch { }
            try
            {
                PageSize = Convert.ToInt32(CollType.GetProperty("PageSize").GetValue(ObjectsCollection));
            }
            catch { }
            try
            {
                TotalCount = Convert.ToInt32(CollType.GetProperty("TotalCount").GetValue(ObjectsCollection));
            }
            catch { }

            IList Objects = (IList)ObjectsCollection;
            TagBuilder table = new TagBuilder("table");
            table.Attributes.Add("id", id);

            TagBuilder header = new TagBuilder("thead");
            TagBuilder headRow = new TagBuilder("tr");
            Dictionary<string, string> FieldMasks = new Dictionary<string, string>();
            foreach (string s in tableSchema)
            {
                string[] hPair = s.Split(':');
                TagBuilder headerCell = new TagBuilder("th");
                headerCell.Attributes.Add("data-field", hPair[0]);
                headerCell.InnerHtml += hPair[1];
                headRow.InnerHtml += headerCell;
                if (!string.IsNullOrEmpty(hPair[0]))
                {
                    try
                    {
                        FieldMasks.Add(hPair[0], (hPair.Length >= 4) ? hPair[3] : "");
                    }
                    catch { }
                }
            }
            header.InnerHtml += headRow.ToString(TagRenderMode.Normal);
            table.InnerHtml += header.ToString(TagRenderMode.Normal);

            TagBuilder body = new TagBuilder("tbody");

            foreach (object obj in Objects)
            {
                TagBuilder contentTr = new TagBuilder("tr");

                Type type = obj.GetType();
                PropertyInfo[] properties = type.GetProperties();
                Dictionary<string, string> props = new Dictionary<string, string>();
                foreach (PropertyInfo property in properties)
                {
                    string pName = property.Name;
                    string pValue = string.Empty;

                    string mask = string.Empty;
                    try
                    {
                        mask = FieldMasks[pName];
                    }
                    catch { }
                    if (property.GetValue(obj, null) == null)
                        pValue = string.Empty;
                    else if (property.GetValue(obj, null).GetType() == typeof(DateTime))
                        pValue = Convert.ToDateTime(property.GetValue(obj, null)).ToString(mask);
                    else if (property.GetValue(obj, null).GetType() == typeof(decimal))
                        pValue = Convert.ToDecimal(property.GetValue(obj, null)).ToString(mask);
                    else if (property.GetValue(obj, null).GetType() == typeof(int))
                        pValue = Convert.ToInt32(property.GetValue(obj, null)).ToString(mask);
                    else
                        pValue = property.GetValue(obj, null).ToString();

                    props.Add(pName, pValue);                    
                }

                foreach (string s in tableSchema)
                {
                    string[] hPair = s.Split(':');
                    string innerHTML = string.Empty;
                    try
                    {
                        if (hPair.Length >= 3)
                        {
                            innerHTML = hPair[2];
                            foreach (Match ItemMatch in Regex.Matches(hPair[2], @"\[.*?\]"))
                            {
                                try
                                {
                                    innerHTML = innerHTML.Replace(ItemMatch.Value, props[ItemMatch.Value.Replace("[", string.Empty).Replace("]", string.Empty)]);
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            innerHTML = props[hPair[0]];
                        }
                    }
                    catch {
                        
                    }
                    TagBuilder bodyCell = new TagBuilder("td");
                    bodyCell.InnerHtml += innerHTML;
                    contentTr.InnerHtml += bodyCell;
                }

                body.InnerHtml += contentTr;
            }

            table.InnerHtml+=body;

            TagBuilder footer = new TagBuilder("tfoot");
            if(PageNumber>0 && PageSize>0)
                footer.InnerHtml += "<tr><td colspan='"+tableSchema.Length.ToString()+"'>" + Paginator.PaginatorControl(html, pageUrl, "page", id + "paginator", PageNumber, PageSize, TotalCount, OnPageChageFunction) + "</tr></td>";
            table.InnerHtml += footer.ToString(TagRenderMode.Normal);
            
            if (!string.IsNullOrEmpty(tableCSS))
                table.AddCssClass(tableCSS);

            return new MvcHtmlString(table.ToString(TagRenderMode.Normal));
        }
    }
}