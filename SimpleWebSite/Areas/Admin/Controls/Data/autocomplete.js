﻿function LoadAutoComplete(obj, id, jsonFile, fieldToFind, valField, template, lim, minLng, autocompletedFunc, selectedFunc) {

    var prefetchObj =
        (fieldToFind == "" || fieldToFind == null) ?
                {
                    url: jsonFile,
                    cacheKey: id,
                    filter: function (list) {
                        return $.map(list, function (country) { return { name: country }; });
                    }
                }
        :
                {
                    url: jsonFile,
                    cacheKey: id
                };
    //alert(JSON.stringify(prefetchObj));
    fieldToFind = (fieldToFind == "" || fieldToFind == null) ? "name" : fieldToFind;

    var container = new Bloodhound({
        name: id,
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace(fieldToFind),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: lim,
        prefetch: prefetchObj
    });
    container.clearPrefetchCache();
    container.initialize();

    $(obj).typeahead("destroy");

    var mytypehead = $(obj).typeahead(
        {
            minLength: minLng
        },
        {
            name: id,
            displayKey: fieldToFind,
            valueKey: valField,
            source: container.ttAdapter(),
            templates: {
                suggestion: Handlebars.compile(template)
            }            
        });

    if(autocompletedFunc!=null && autocompletedFunc!=""){
        mytypehead.on('typeahead:autocompleted', eval(autocompletedFunc));
    }
    if(selectedFunc!=null && selectedFunc!=""){
        mytypehead.on('typeahead:selected', eval(selectedFunc));
    }    
}
