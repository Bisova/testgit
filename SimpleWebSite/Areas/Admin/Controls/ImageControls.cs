﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleWebSiteLibrary;

namespace SimpleWebSite.Areas.Admin.Controls
{
    public static class ImageControls
    {
        /// <summary>
        /// Элемент вывода Изображение Fancybox 
        /// Требует подключения Javascript-библиотек: jquery.fancybox.js, CommonScripts.js
        /// Требует поключения css-библиотеки классов: jquery.fancybox.css
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элеметна ссылки</param>
        /// <param name="imgCss">CSS-класс тэга изображения</param>
        /// <param name="imageUrl">Путь к файлу изображения</param>
        /// <param name="imgStyle">Атрибут style тэга изображения</param>
        /// <param name="previewImageUrl">Путь к файлу превью изображения</param>
        /// <param name="linkCss">Css-класс ссылки-обертки изображения</param>
        /// <param name="imgAlt">alt-атрибут тэга изображения</param>
        /// <returns>Возвращает элемент вывода Изображение Fancybox</returns>
        public static MvcHtmlString ImageFancyBoxInput(this HtmlHelper html, string id, string imageUrl, 
            string previewImageUrl, string linkCss, string imgCss, string imgStyle, string imgAlt, string groupName)
        {
            bool imageExists = System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(imageUrl));
            bool imagePreviewExists = System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(previewImageUrl));
            TagBuilder img = new TagBuilder("img");
            img.Attributes.Add("alt", imgAlt);
            if(!string.IsNullOrEmpty(imgCss))
                img.AddCssClass(imgCss);
            if (!string.IsNullOrEmpty(imgStyle))
                img.Attributes.Add("style", imgStyle);
            img.Attributes.Add("src", previewImageUrl);

            if (imageExists)
            {
                TagBuilder link = new TagBuilder("a");
                link.Attributes.Add("href", imageUrl);
                if(!string.IsNullOrEmpty(linkCss))
                    link.AddCssClass(linkCss);
                link.AddCssClass("fancybox");
                if (!string.IsNullOrEmpty(groupName))
                    link.Attributes.Add("rel", groupName);
                link.InnerHtml += img.ToString(TagRenderMode.SelfClosing);

                return new MvcHtmlString(link.ToString(TagRenderMode.Normal));
            }
            else
            {
                if (imagePreviewExists)
                {
                    return new MvcHtmlString(img.ToString(TagRenderMode.SelfClosing));
                }
                else
                    return new MvcHtmlString("");
            }
        }

        public static MvcHtmlString ImageDraggableCollection(this HtmlHelper html, string id, PictureFileCollection images,
            string containerCss, string imageContainerCss, string imageLinkCss, string imageCss, string ImageKey, string ImagePreviewKey)
        {
            TagBuilder mainContainer = new TagBuilder("div");
            mainContainer.Attributes.Add("id", id);

            if(!string.IsNullOrEmpty(containerCss))
                mainContainer.AddCssClass(containerCss);

            foreach (PictureFile img in images)
            {
                TagBuilder imageContainer = new TagBuilder("div");
                if(!string.IsNullOrEmpty(imageContainerCss))
                    imageContainer.AddCssClass(imageContainerCss);

                imageContainer.InnerHtml += ImageFancyBoxInput(html, "Image_" + img.id.ToString(), img.imagePathes[ImageKey],
                    img.imagePathes[ImagePreviewKey], imageLinkCss, imageCss, "", "", id);

                mainContainer.InnerHtml += imageContainer;
            }

            return new MvcHtmlString(mainContainer.ToString(TagRenderMode.Normal));
        }



        #region<<ImageCarousel>>

        private static MvcHtmlString ImageCarouselItem(this HtmlHelper html, bool isActive, string ImageSrc, 
            string ImageAlt, string CarouselCaption, string ItemStyle)
        {
            TagBuilder Item = new TagBuilder("div");
            Item.AddCssClass("item");
            if (isActive)
                Item.AddCssClass("active");
            if (!string.IsNullOrEmpty(ItemStyle))
                Item.Attributes.Add("style", ItemStyle);

            TagBuilder Img = new TagBuilder("img");
            Img.Attributes.Add("src", ImageSrc);
            Img.Attributes.Add("alt", ImageAlt);

            TagBuilder Caption = new TagBuilder("div");
            Caption.AddCssClass("carousel-caption");
            Caption.InnerHtml += CarouselCaption;

            Item.InnerHtml += Img.ToString(TagRenderMode.SelfClosing);
            if(!string.IsNullOrEmpty(CarouselCaption))
                Item.InnerHtml += Caption;

            return new MvcHtmlString(Item.ToString(TagRenderMode.Normal));

        }
        /*var img = $('<img />').attr('src', 'http://somedomain.com/image.jpg').load(function() {if (!this.complete || typeof this.naturalWidth == 'undefined' || this.naturalWidth == 0) {alert('broken image!');}else {$('#"+id+"').append(img);}});*/
        private static MvcHtmlString ImageCarousel(this HtmlHelper html, string id, List<Dictionary<string,string>> images, 
            bool ShowIndicators, bool ShowControls, string LeftSpanClass, string RightSpanClass,
            int interval, bool pauseOnHover, bool wrap, string ItemStyle, bool lazy, int LoadOnStart)
        {
            string query = string.Empty;
            if (lazy && LoadOnStart-1 < images.Count)
            {
                string JSON = Newtonsoft.Json.JsonConvert.SerializeObject(images);

                query =
                "var obj" + id + " = JSON.parse('" + JSON + "');" +
                "for(var i="+LoadOnStart.ToString()+";i<="+(images.Count-1).ToString()+";i++){" +
                "var img" + id + " = $('<img />').attr('src', obj" + id + "[i].src).attr('data-index',i);" +
                "$(img" + id + ").load(function() {if (!this.complete || typeof this.naturalWidth == 'undefined' || this.naturalWidth == 0) {alert('broken image!');}else {"+
                "$('#" + id + "').find('ol.carousel-indicators').append($('<li data-slide-to=\"'+$(this).attr('data-index')+'\" data-target=\"#"+id+"\"></li>'));" +
                "$('#" + id + "').find('div.carousel-inner').append($('<div class=\"item\" style=\"" + ItemStyle + "\"></div>').html(/*$(img)*/$('<img src=\"'+$(this).attr('src')+'\"/>')));}});}";

            }
            
            string dataInterval = (interval <= 0) ? "false" : interval.ToString();
            string pause = (pauseOnHover) ? "true" : "false";
            TagBuilder Script = new TagBuilder("script");
            Script.Attributes.Add("type", "text/javascript");
            Script.InnerHtml +=
                "$(document).ready(function(){"+
                    "$('#"+id+"').carousel();" +
                //"});"+
                query+
                /**/
                "});";

            LeftSpanClass = (string.IsNullOrEmpty(LeftSpanClass))?"glyphicon glyphicon-chevron-left":LeftSpanClass;
            RightSpanClass = (string.IsNullOrEmpty(RightSpanClass))?"glyphicon glyphicon-chevron-right":RightSpanClass;
            
            if (string.IsNullOrEmpty(id))
                return new MvcHtmlString("");

            TagBuilder Carousel = new TagBuilder("div");
            Carousel.Attributes.Add("id", id);
            Carousel.Attributes.Add("data-interval", dataInterval);
            Carousel.AddCssClass("carousel");
            Carousel.AddCssClass("slide");
            Carousel.Attributes.Add("data-ride", "carousel");
            Carousel.Attributes.Add("data-wrap", wrap.ToString().ToLower());
            Carousel.Attributes.Add("data-pause", pause);

            if (ShowIndicators)
            {
                TagBuilder Indicators = new TagBuilder("ol");
                Indicators.AddCssClass("carousel-indicators");
                for (int i = 0; i < images.Count; i++)
                {
                    if (i > LoadOnStart - 1)
                        break;
                    TagBuilder IndicatorItem = new TagBuilder("li");
                    IndicatorItem.Attributes.Add("data-target", "#" + id);
                    IndicatorItem.Attributes.Add("data-slide-to", i.ToString());
                    if (i == 0)
                        IndicatorItem.AddCssClass("active");
                    Indicators.InnerHtml += IndicatorItem;
                }
                Carousel.InnerHtml += Indicators;
            }

            TagBuilder ItemContainer = new TagBuilder("div");
            ItemContainer.AddCssClass("carousel-inner");

            int n = 0;
            foreach (Dictionary<string,string> item in images)
            {
                if (n > LoadOnStart - 1)
                    break;
                try
                {
                    bool flag = (n == 0) ? true : false;
                    string src = (item.ContainsKey("src")) ? item["src"] : "";
                    string alt = (item.ContainsKey("alt")) ? item["alt"] : "";
                    string caption = (item.ContainsKey("caption")) ? item["caption"] : "";
                    ItemContainer.InnerHtml += ImageCarouselItem(html, flag, src, alt, caption, ItemStyle);
                    n++;
                }
                catch { }
            }

            Carousel.InnerHtml += ItemContainer.ToString(TagRenderMode.Normal);
            
            if(ShowControls){
                //Controls
                TagBuilder Left = new TagBuilder("a");
                Left.AddCssClass("left");
                Left.AddCssClass("carousel-control");
                Left.Attributes.Add("href", "#" + id);
                Left.Attributes.Add("role", "button");
                Left.Attributes.Add("data-slide", "prev");

                TagBuilder LeftSpan = new TagBuilder("span");
                LeftSpan.AddCssClass(LeftSpanClass);

                Left.InnerHtml+=LeftSpan;

                Carousel.InnerHtml += Left;

                TagBuilder Right = new TagBuilder("a");
                Right.AddCssClass("right");
                Right.AddCssClass("carousel-control");
                Right.Attributes.Add("href", "#" + id);
                Right.Attributes.Add("role", "button");
                Right.Attributes.Add("data-slide", "next");

                TagBuilder RightSpan = new TagBuilder("span");
                RightSpan.AddCssClass(RightSpanClass);

                Right.InnerHtml+=RightSpan;

                Carousel.InnerHtml += Right;
            }

            return new MvcHtmlString(Script.ToString()+Carousel.ToString());
        }
        /// <summary>
        /// Элемент вывода "Карусель"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента вывода на странице</param>
        /// <param name="images">Набор изображений (Dictionary: src, alt, caption)</param>
        /// <param name="ShowIndicators">Показывать ли индикаторы (элемент выбора изображения)</param>
        /// <param name="ShowControls">Показывать ли элементы прокрутки изображений</param>
        /// <param name="LeftSpanClass">Иконка левого элемента прокрутки</param>
        /// <param name="RightSpanClass">Иконка правого элемента прокрутки</param>
        /// <param name="interval">Интенсивность прокрутки в милисекундах, если 0, то не прокручивается</param>
        /// <param name="pauseOnHover">Останавливать ли прокрутку при наведении мыши</param>
        /// <param name="wrap">Whether the carousel should cycle continuously or have hard stops.</param>
        /// <param name="ItemStyle">Тэг style элемента карусели div.item</param>
        /// <param name="LoadOnStart">Сколько изображений загружать сразу (остальные подгружаются после загрузки страницы)</param>
        /// <returns>Возвращает элемент управления "Карусель"</returns>
        public static MvcHtmlString ImageCarousel(this HtmlHelper html, string id, List<Dictionary<string, string>> images,
            bool ShowIndicators, bool ShowControls, string LeftSpanClass, string RightSpanClass,
            int interval, bool pauseOnHover, bool wrap, string ItemStyle, int LoadOnStart)
        {
            return ImageCarousel(html, id, images, ShowIndicators, ShowControls, LeftSpanClass, RightSpanClass, interval, pauseOnHover, wrap, ItemStyle, true, LoadOnStart);
        }
        /// <summary>
        /// Элемент вывода "Карусель"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента вывода на странице</param>
        /// <param name="images">Набор изображений (Dictionary: src, alt, caption)</param>
        /// <param name="ShowIndicators">Показывать ли индикаторы (элемент выбора изображения)</param>
        /// <param name="ShowControls">Показывать ли элементы прокрутки изображений</param>
        /// <param name="LeftSpanClass">Иконка левого элемента прокрутки</param>
        /// <param name="RightSpanClass">Иконка правого элемента прокрутки</param>
        /// <param name="interval">Интенсивность прокрутки в милисекундах, если 0, то не прокручивается</param>
        /// <param name="pauseOnHover">Останавливать ли прокрутку при наведении мыши</param>
        /// <param name="wrap">Whether the carousel should cycle continuously or have hard stops.</param>
        /// <param name="ItemStyle">Тэг style элемента карусели div.item</param>
        /// <param name="LoadOnStart">Сколько изображений загружать сразу (остальные подгружаются после загрузки страницы)</param>
        /// <returns>Возвращает элемент управления "Карусель"</returns>
        public static MvcHtmlString ImageCarousel(this HtmlHelper html, string id, List<Dictionary<string, string>> images,
            bool ShowIndicators, bool ShowControls, string LeftSpanClass, string RightSpanClass,
            int interval, bool pauseOnHover, bool wrap, string ItemStyle)
        {
            return ImageCarousel(html, id, images, ShowIndicators, ShowControls, LeftSpanClass, RightSpanClass, interval, pauseOnHover, wrap, ItemStyle, false, images.Count+1);
        }

        #endregion

    }
}