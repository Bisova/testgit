﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleWebSite.Areas.Admin.Controls
{
    public static class InputControls
    {
        /// <summary>
        /// Элемент ввода данных "Календарь"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента ввода</param>
        /// <param name="type">Тип элемента ввода</param>
        /// <param name="size">Размер элемента: small, normall, large (по-умолчанию normall)</param>
        /// <param name="AddOn">Размещение элемента префикса(left-слева, right-справа, none-не отображать)</param>
        /// <param name="AddOnText">Текста на элементе префиксе</param>
        /// <param name="inputCSS">css-класс элемента ввода</param>
        /// <param name="placeholder">Placeholder элемента ввода</param>
        /// <param name="value">Содержание(текст)элемента ввода</param>
        /// <param name="attributes">Атрибуты элемента ввода данных</param>
        /// <returns>Возвращает элемент ввода данных "Календарь"</returns>
        public static MvcHtmlString SimpleInput(this HtmlHelper html, string id, string type, string size, string inputCSS, string AddOn, string AddOnText, string placeholder, string value, Dictionary<string,string> attributes)
        {
            string[] sizes = new string[] { "small", "normal", "large" };
            size = (string.IsNullOrEmpty(size) || !sizes.Contains(size)) ? "normal" : size;
            TagBuilder container = new TagBuilder("div");
            container.AddCssClass("input-group");
            
            if(size=="large")
                container.AddCssClass("input-group-lg");
            if (size == "small")
                container.AddCssClass("input-group-sm");
            if (!string.IsNullOrEmpty(inputCSS))
                container.AddCssClass(inputCSS);

            TagBuilder input = new TagBuilder("input");
            input.AddCssClass("form-control");
            input.Attributes.Add("id", id);
            input.Attributes.Add("type", type);

            if (attributes != null)
            {
                foreach (KeyValuePair<string, string> entry in attributes)
                {
                    if (entry.Key != "type" && entry.Key != "class" && entry.Key != "id" && entry.Key != "value" && entry.Key != "placeholder")
                    {
                        input.Attributes.Remove(entry.Key);
                        input.Attributes.Add(entry.Key, entry.Value);
                    }
                }
            }

            if (!string.IsNullOrEmpty(placeholder))
                input.Attributes.Add("placeholder", placeholder);
            if(!string.IsNullOrEmpty(value))
                input.Attributes.Add("value",value);

            if (AddOn != "none")
            {
                AddOn = (AddOn != "left" && AddOn != "right") ? "left" : AddOn;
                TagBuilder addOn = new TagBuilder("span");
                addOn.AddCssClass("input-group-addon");
                addOn.InnerHtml = AddOnText;

                if (AddOn == "left")
                {
                    container.InnerHtml += addOn;
                    container.InnerHtml += input.ToString(TagRenderMode.SelfClosing);
                }
                if (AddOn == "right")
                {
                    container.InnerHtml += input.ToString(TagRenderMode.SelfClosing);
                    container.InnerHtml += addOn;
                }
            }
            else
            {
                container.InnerHtml += input.ToString(TagRenderMode.SelfClosing);
            }

            return new MvcHtmlString(container.ToString( TagRenderMode.Normal));
        }
        /// <summary>
        /// Элемент ввода данных "Календарь"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента ввода</param>
        /// <param name="type">Тип элемента ввода</param>
        /// <param name="size">Размер элемента: small, normall, large (по-умолчанию normall)</param>
        /// <param name="AddOn">Размещение элемента префикса(left-слева, right-справа, none-не отображать)</param>
        /// <param name="AddOnText">Текста на элементе префиксе</param>
        /// <param name="inputCSS">css-класс элемента ввода</param>
        /// <param name="placeholder">Placeholder элемента ввода</param>
        /// <param name="value">Содержание(текст)элемента ввода</param>
        /// <param name="attributes">Атрибуты элемента ввода данных</param>
        /// <returns>Возвращает элемент ввода данных "Календарь"</returns>
        public static MvcHtmlString SimpleInput(this HtmlHelper html, string id, string size, string inputCSS, string AddOn, string AddOnText, string placeholder, string value, Dictionary<string, string> attributes)
        {
            return SimpleInput(html, id, "text", size, inputCSS, AddOn, AddOnText, placeholder, value, attributes);
        }
        /// <summary>
        /// Элемент ввода "Календарь"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента ввода на странице</param>
        /// <param name="size">Размер элемента: small, normall, large (по-умолчанию normall)</param>
        /// <param name="inputCSS">css-класс элемента ввода</param>
        /// <param name="AddOn">Размещение элемента префикса(left-слева, right-справа, none-не отображать)</param>
        /// <param name="placeholder">Placeholder элемента ввода</param>
        /// <param name="value">Содержание(текст)элемента ввода</param>
        /// <param name="dateMask">Маска даты</param>
        /// <param name="todayHighlight">Подсвечивать ли текущую дату</param>
        /// <param name="startDate">Начальная точка допустимого диапазона дат</param>
        /// <param name="endDate">Конечная точка допустимого диапазона дат</param>
        /// <returns>Возвращает элемент ввода "Календарь"</returns>
        public static MvcHtmlString CalendarInput(this HtmlHelper html, string id, string size, string inputCSS, string AddOn, string placeholder, string value, string dateMask, bool todayHighlight, DateTime startDate, DateTime endDate)
        {
            dateMask = (string.IsNullOrEmpty(dateMask)) ? "dd.MM.yyyy" : dateMask.Replace("mm","MM");
            TagBuilder script = new TagBuilder("script");
            script.Attributes.Add("type", "text/javascript");

            string today = (todayHighlight) ? "todayHighlight:true," : "";
            string start = (startDate!=DateTime.MinValue) ? "startDate:'"+startDate.ToString(dateMask)+"'," : "";

            string end = (endDate!=DateTime.MinValue) ? "endDate:'"+endDate.ToString(dateMask)+"'," : "";

            script.InnerHtml =
                "$(document).ready(function(){" +
                    "$('#" + id + "').datepicker({" +
                    "format: '" + dateMask.ToLower() + "'," +
                    "language: 'ru'," +
                    today +
                    start +
                    end +
                    "autoclose: true" +
                    "});";
            if (AddOn != "none")
            {
                script.InnerHtml += (AddOn=="left")
                    ?"$('#" + id + "').prev().on('click',function(){$('#" + id + "').focus();});"
                    : "$('#" + id + "').next().on('click',function(){$('#" + id + "').focus();});";
            }

            script.InnerHtml+="});";

            return new MvcHtmlString(script.ToString( TagRenderMode.Normal)+
                SimpleInput(html, id, "text", size, inputCSS, AddOn, "<i class='fa fa-calendar'></i>", placeholder, value, null));
        }
        /// <summary>
        /// Элемент ввода "Номер телефона"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента ввода данных</param>
        /// <param name="size">Размер элемента: small, normall, large (по-умолчанию normall)</param>
        /// <param name="inputCSS">css-класс элемента ввода</param>
        /// <param name="AddOn">Размещение элемента префикса(left-слева, right-справа, none-не отображать)</param>
        /// <param name="placeholder">Placeholder элемента ввода</param>
        /// <param name="value">Содержание(текст)элемента ввода</param>
        /// <param name="phoneMask">Маска номера телефона в формате +7 (ddd)ddd-dd-dd</param>
        /// <returns>Возвращает элемент ввода "Номер телефона"</returns>
        public static MvcHtmlString PhoneInput(this HtmlHelper html, string id, string size, string inputCSS, string AddOn, string placeholder, string value, string phoneMask)
        {
            TagBuilder script = new TagBuilder("script");
            script.Attributes.Add("type", "text/javascript");

            script.InnerHtml =
                "$(document).ready(function(){" +
                    "$('#" + id + "').bfhphone($('#" + id + "').data());";
            script.InnerHtml += "});";

            Dictionary<string, string> atrs = new Dictionary<string, string>();
            atrs.Add("data-format", phoneMask);
            atrs.Add("data-number", value);

            return new MvcHtmlString(script.ToString(TagRenderMode.Normal) +
                SimpleInput(html, id, "tel", size, "bfh-phone "+inputCSS, AddOn, "<i class='fa fa-phone'></i>", placeholder, string.Empty, atrs));

        }
        /// <summary>
        /// Элемент ввода с автозаполнением
        /// Требует подключения js-библиотек:bloodhound.min.js,typeahead.jquery.min.js, handlebars-v1.3.0.js, autocomplete.js
        /// Требует подключения css-библиотеки typeahead.css
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код контейнера</param>
        /// <param name="containerCss">CSS-класс контейнера элемента ввода</param>
        /// <param name="inputCss">CSS-класс элемента ввода</param>
        /// <returns>Возвращает элемент ввода с автозаполнением</returns>
        private static MvcHtmlString AutoCompleteInput(this HtmlHelper html, string id, string containerCss, string inputCss, string placeholder,
            string jsonFile, string displayKey, string valueKey, string template, int limit, int minLength, string cachePrefix,
            string autocompleteFunc, string selectedFunc)
        {

            TagBuilder Script = new TagBuilder("script");
            Script.Attributes.Add("type", "text/javascript");
            Script.InnerHtml +=
                "$(document).ready(function(){" +
                    "LoadAutoComplete($('#" + id + " .typeahead'), '" + cachePrefix + "', '" + jsonFile + "', '" + displayKey + "', '" + valueKey + "', '" + template + "', " + limit + ", " + minLength + ", '"+autocompleteFunc+"', '"+selectedFunc+"');" +
                "});";

            TagBuilder container = new TagBuilder("div");
            container.Attributes.Add("id", id);
            container.AddCssClass(containerCss);

            TagBuilder input = new TagBuilder("input");
            input.AddCssClass(inputCss);
            input.Attributes.Add("type", "text");
            input.AddCssClass("typeahead");
            input.Attributes.Add("placeholder", placeholder);

            container.InnerHtml += input.ToString(TagRenderMode.SelfClosing);

            return new MvcHtmlString(Script.ToString(TagRenderMode.Normal) +
                container.ToString(TagRenderMode.Normal)
                );
        }

        /// <summary>
        /// Элемент ввода с автозаполнением
        /// Требует подключения js-библиотек:bloodhound.min.js,typeahead.jquery.min.js, handlebars-v1.3.0.js, autocomplete.js
        /// Требует подключения css-библиотеки typeahead.css
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код контейнера</param>
        /// <param name="containerCss">CSS-класс контейнера элемента ввода</param>
        /// <param name="inputCss">CSS-класс элемента ввода</param>
        /// <returns>Возвращает элемент ввода с автозаполнением</returns>
        public static MvcHtmlString AutoCompleteInput(this HtmlHelper html, string id, string containerCss, string inputCss, string placeholder,
            string jsonFile, string displayKey, string valueKey, string template, int limit, int minLength, string autocompleteFunc, string selectedFunc)
        {
            string AbsoluteFilePath = System.Web.HttpContext.Current.Server.MapPath(jsonFile);
            string cachePrefix = string.Empty;
            if (System.IO.File.Exists(AbsoluteFilePath))
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(AbsoluteFilePath);
                cachePrefix = fi.Name.Replace(fi.Extension, "");
            }
            else
            {
                TagBuilder txtInput = new TagBuilder("input");
                txtInput.AddCssClass(inputCss);
                txtInput.Attributes.Add("id", id);
                return new MvcHtmlString(txtInput.ToString(TagRenderMode.SelfClosing));
            }

            return AutoCompleteInput(html, id, containerCss, inputCss, placeholder, jsonFile, displayKey, valueKey, template, limit, minLength, cachePrefix, autocompleteFunc, selectedFunc);
        }


        /// <summary>
        /// Элемент ввода с автозаполнением
        /// Требует подключения js-библиотек:bloodhound.min.js,typeahead.jquery.min.js, handlebars-v1.3.0.js, autocomplete.js
        /// Требует подключения css-библиотеки typeahead.css
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код контейнера</param>
        /// <param name="containerCss">CSS-класс контейнера элемента ввода</param>
        /// <param name="inputCss">CSS-класс элемента ввода</param>
        /// <returns>Возвращает элемент ввода с автозаполнением</returns>
        public static MvcHtmlString AutoCompleteInput(this HtmlHelper html, string id, string containerCss, string inputCss, string placeholder,
            string jsonFile, string displayKey, string valueKey, string template, int limit, int minLength)
        {
            string AbsoluteFilePath = System.Web.HttpContext.Current.Server.MapPath(jsonFile);
            string cachePrefix = string.Empty;
            if (System.IO.File.Exists(AbsoluteFilePath))
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(AbsoluteFilePath);
                cachePrefix = fi.Name.Replace(fi.Extension, "");
            }
            else
            {
                TagBuilder txtInput = new TagBuilder("input");
                txtInput.AddCssClass(inputCss);
                txtInput.Attributes.Add("id", id);
                return new MvcHtmlString(txtInput.ToString(TagRenderMode.SelfClosing));
            }

            return AutoCompleteInput(html, id, containerCss, inputCss, placeholder, jsonFile, displayKey, valueKey, template, limit, minLength, "","");
        }

        /// <summary>
        /// Элемент ввода с автозаполнением
        /// Требует подключения js-библиотек:bloodhound.min.js,typeahead.jquery.min.js, handlebars-v1.3.0.js, autocomplete.js
        /// Требует подключения css-библиотеки typeahead.css
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код контейнера</param>
        /// <param name="containerCss">CSS-класс контейнера элемента ввода</param>
        /// <param name="inputCss">CSS-класс элемента ввода</param>
        /// <returns>Возвращает элемент ввода с автозаполнением</returns>
        public static MvcHtmlString AutoCompleteInput(this HtmlHelper html, string id, string containerCss, string inputCss, string placeholder,
            string cachePrefix, string jsonUrl, string displayKey, string valueKey, string template, int limit, int minLength,
            string autocompleteFunc, string selectedFunc)
        {
            return AutoCompleteInput(html, id, containerCss, inputCss, placeholder, jsonUrl, displayKey, valueKey, template, limit, minLength, cachePrefix, autocompleteFunc, selectedFunc);
        }

        /// <summary>
        /// Элемент ввода с автозаполнением
        /// Требует подключения js-библиотек:bloodhound.min.js,typeahead.jquery.min.js, handlebars-v1.3.0.js, autocomplete.js
        /// Требует подключения css-библиотеки typeahead.css
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код контейнера</param>
        /// <param name="containerCss">CSS-класс контейнера элемента ввода</param>
        /// <param name="inputCss">CSS-класс элемента ввода</param>
        /// <returns>Возвращает элемент ввода с автозаполнением</returns>
        public static MvcHtmlString AutoCompleteInput(this HtmlHelper html, string id, string containerCss, string inputCss, string placeholder,
            string cachePrefix, string jsonUrl, string displayKey, string valueKey, string template, int limit, int minLength)
        {
            return AutoCompleteInput(html, id, containerCss, inputCss, placeholder, jsonUrl, displayKey, valueKey, template, limit, minLength, cachePrefix, "", "");
        }

        /// <summary>
        /// Элемент ввода текстовое поле с автроизменением по высоте
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента на странице</param>
        /// <param name="value">Содержание текствого поля</param>
        /// <param name="cssClass">CSS-класс текстового поля</param>
        /// <param name="attributes">Атрибуты текстового поля</param>
        /// <returns>Возвращает элемент ввода текстовое поле с автоматическим изменением по высоте</returns>
        public static MvcHtmlString AutoSizeTextArea(this HtmlHelper html, string id, string value, string cssClass, Dictionary<string, string> attributes)
        {
            if (string.IsNullOrEmpty(id))
                return new MvcHtmlString("");

            TagBuilder Script = new TagBuilder("script");
            Script.Attributes.Add("type", "text/javascript");
            Script.InnerHtml += 
                "$(document).ready(function(){"+
                    "$('#"+id+"').autosize();"+    
                "});";

            TagBuilder textarea = new TagBuilder("textarea");
            textarea.Attributes.Add("id", id);
            if (attributes != null)
            {
                foreach (KeyValuePair<string, string> attr in attributes)
                {
                    textarea.Attributes.Remove(attr.Key);
                    textarea.Attributes.Add(attr.Key, attr.Value);
                }
            }
            textarea.AddCssClass(cssClass);
            textarea.InnerHtml += value;

            return new MvcHtmlString(
                Script.ToString(TagRenderMode.Normal) +
                textarea.ToString(TagRenderMode.Normal));
        }
        /// <summary>
        /// Элемент ввода CKEDITOR
        /// Требует подлкючения js-библиотек: ckeditor.js
        /// </summary>
        /// <param name="html"></param>
        /// <param name="id">Код элемента ввода на странице</param>
        /// <param name="value">Содержание элемента ввода</param>
        /// <returns>Возвращает элемент ввода CKEDITOR</returns>
        public static MvcHtmlString CKEditorInput(this HtmlHelper html, string id, string value)
        {
            TagBuilder Script = new TagBuilder("script");
            Script.Attributes.Add("type", "text/javascript");
            Script.InnerHtml +=
                "$(document).ready(function(){" +
                    "CKEDITOR.replace('"+id + "');" +
                "});";

            TagBuilder textarea = new TagBuilder("textarea");
            textarea.Attributes.Add("id", id);
            textarea.InnerHtml += value;

            return new MvcHtmlString(
                Script.ToString(TagRenderMode.Normal) +
                textarea.ToString(TagRenderMode.Normal));

        }
    }
}