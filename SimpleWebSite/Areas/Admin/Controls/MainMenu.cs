﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;

namespace SimpleWebSite.Areas.Admin.Controls
{
    public static class MainMenu
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="DataFilePath">Путь к файлу с данными меню</param>
        /// <param name="colorScheme">Цветовая схема (dark - темная, light - светлая)</param>
        /// <returns>Возвращает меню бар</returns>
        public static MvcHtmlString MainMenuControl(this HtmlHelper html, string id, string DataFilePath, string colorScheme)
        {
            string CurrentUserLogin = (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)?System.Web.HttpContext.Current.User.Identity.Name:"";
            string pathToFile = System.Web.HttpContext.Current.Server.MapPath(DataFilePath);
            if (!File.Exists(pathToFile))
                return new MvcHtmlString(string.Empty);

            XmlDocument doc = new XmlDocument();
            doc.Load(pathToFile);
            
            XmlElement root = doc.DocumentElement;
            if(root==null)
                return new MvcHtmlString(string.Empty);

            /*Блок навигации*/
            TagBuilder nav = new TagBuilder("nav");
            nav.AddCssClass("navbar");
            if (colorScheme == "dark")
                nav.AddCssClass("navbar-inverse");
            else
                nav.AddCssClass("navbar-default");
            /*Блок навигации--окончание*/

            /*Главный контейнер*/
            TagBuilder mainContainer = new TagBuilder("div");
            mainContainer.AddCssClass("container-fluid");
            /*Главный контейнер--окончание*/

            /*Header block*/
            TagBuilder header = new TagBuilder("div");
            header.AddCssClass("navbar-header");

            TagBuilder ToggleMenuBtn = new TagBuilder("button");
            ToggleMenuBtn.Attributes.Add("type", "button");
            ToggleMenuBtn.Attributes.Add("class", "navbar-toggle");
            ToggleMenuBtn.Attributes.Add("data-toggle", "collapse");
            ToggleMenuBtn.Attributes.Add("data-target", "#"+id);
            ToggleMenuBtn.InnerHtml = "<span class='sr-only'>Toggle navigation</span><span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>";
            header.InnerHtml += ToggleMenuBtn;
            /*Header block--окончание*/

            /*Menu Container*/
            TagBuilder MenuContainer = new TagBuilder("div");
            MenuContainer.AddCssClass("collapse");
            MenuContainer.AddCssClass("navbar-collapse");
            MenuContainer.Attributes.Add("id",id);

            XmlNodeList nodes = root.SelectNodes("block");

            foreach (XmlNode node in nodes)
            {
                if (node.Attributes["visible"] == null || node.Attributes["visible"].Value.ToLower() == "true")
                {
                    if (node.Attributes["type"] != null)
                    {
                        if (node.Attributes["type"].Value == "header")
                        {
                            TagBuilder headerLink = new TagBuilder("a");
                            headerLink.AddCssClass("navbar-brand");
                            if (node.Attributes["href"] != null)
                                headerLink.Attributes.Add("href", node.Attributes["href"].Value);
                            if (node.Attributes["onclick"] != null)
                                headerLink.Attributes.Add("onclick", node.Attributes["onclick"].Value);
                            headerLink.InnerHtml = node.InnerText.Replace("[username]", CurrentUserLogin);
                            header.InnerHtml += headerLink;
                        }
                        else
                        {
                            if (node.Attributes["type"].Value == "container")
                            {
                                TagBuilder listEl = new TagBuilder("ul");
                                string dest = (node.Attributes["align"] == null) ? string.Empty : "navbar-" + node.Attributes["align"].Value;
                                listEl.AddCssClass("navbar-nav");
                                listEl.AddCssClass("nav");
                                if (!string.IsNullOrEmpty(dest))
                                    listEl.AddCssClass(dest);
                                foreach (XmlNode n in node.SelectNodes("element"))
                                {
                                    TagBuilder li = new TagBuilder("li");
                                    TagBuilder liA = new TagBuilder("a");
                                    liA.InnerHtml = n.InnerText.Replace("[username]", CurrentUserLogin);
                                    if (n.Attributes["href"] != null)
                                    {
                                        liA.Attributes.Add("href", n.Attributes["href"].Value);
                                        if (System.Web.HttpContext.Current.Request.RawUrl.Contains(n.Attributes["href"].Value))
                                            li.AddCssClass("active");
                                    }

                                    if (n.Attributes["onclick"] != null)
                                        liA.Attributes.Add("onclick", n.Attributes["onclick"].Value);

                                    if (n.SelectNodes("element").Count > 0)
                                    {
                                        liA.InnerHtml = (((n.Attributes["title"] != null) ? n.Attributes["title"].Value : "") + " <span class='caret'></span>").Replace("[username]", CurrentUserLogin);
                                        liA.Attributes.Remove("href");
                                        liA.Attributes.Add("href", "#");
                                        liA.AddCssClass("dropdown-toggle");
                                        liA.Attributes.Add("data-toggle", "dropdown");

                                        TagBuilder nestUL = new TagBuilder("ul");
                                        nestUL.AddCssClass("dropdown-menu");
                                        nestUL.Attributes.Add("role", "menu");
                                        foreach (XmlNode nestn in n.SelectNodes("element"))
                                        {
                                            if (nestn.Attributes["visible"] == null || nestn.Attributes["visible"].Value.ToLower() == "true")
                                            {
                                                if (nestn.Attributes["type"] != null && nestn.Attributes["type"].Value == "divider")
                                                {
                                                    TagBuilder divider = new TagBuilder("li");
                                                    divider.AddCssClass("divider");
                                                    nestUL.InnerHtml += divider;
                                                }
                                                else
                                                {
                                                    string href =
                                                        (nestn.Attributes["href"] != null && nestn.Attributes["href"].Value.Trim() != string.Empty) ? nestn.Attributes["href"].Value : "#";
                                                    string onclick =
                                                        (nestn.Attributes["onclick"] != null && nestn.Attributes["onclick"].Value.Trim() != string.Empty) ? nestn.Attributes["onclick"].Value : string.Empty;

                                                    TagBuilder nestLi = new TagBuilder("li");
                                                    TagBuilder nestLIA = new TagBuilder("a");
                                                    nestLIA.Attributes.Add("href", href);
                                                    if (onclick != "")
                                                        nestLIA.Attributes.Add("onclick", onclick);
                                                    nestLIA.InnerHtml += nestn.InnerText.Replace("[username]", CurrentUserLogin);
                                                    nestLi.InnerHtml += nestLIA;
                                                    nestUL.InnerHtml += nestLi;
                                                }
                                            }
                                        }
                                        li.InnerHtml += nestUL;
                                    }
                                    li.InnerHtml += liA;
                                    listEl.InnerHtml += li;

                                }
                                MenuContainer.InnerHtml += listEl;
                            }
                            if (node.Attributes["type"].Value == "form")
                            {
                                if (node.Attributes["role"] != null)
                                {
                                    string role = node.Attributes["role"].Value;

                                    TagBuilder formEl = new TagBuilder("form");
                                    string dest = (node.Attributes["align"] == null) ? "navbar-left" : "navbar-" + node.Attributes["align"].Value;
                                    formEl.AddCssClass("navbar-form");
                                    formEl.Attributes.Add("role", role);
                                    if (!string.IsNullOrEmpty(dest))
                                        formEl.AddCssClass(dest);

                                    if (role == "search")
                                    {
                                        string placeholder = (node.Attributes["placeholder"] != null) ? node.Attributes["placeholder"].Value : "";
                                        string buttonText = (node.Attributes["buttonText"] != null) ? node.Attributes["buttonText"].Value : "Submit";

                                        TagBuilder searchCont = new TagBuilder("div");
                                        searchCont.AddCssClass("form-group");

                                        TagBuilder searchInput = new TagBuilder("input");
                                        searchInput.AddCssClass("form-control");
                                        searchInput.Attributes.Add("id", "txtSearch" + id);
                                        searchInput.Attributes.Add("type", "text");
                                        searchInput.Attributes.Add("placeholder", placeholder);
                                        searchCont.InnerHtml += searchInput;
                                        formEl.InnerHtml += searchCont;

                                        TagBuilder seachButton = new TagBuilder("button");
                                        seachButton.AddCssClass("btn btn-default");
                                        seachButton.Attributes.Add("type", "submit");
                                        seachButton.InnerHtml = buttonText;
                                        seachButton.Attributes.Add("id", "searchButton" + id);

                                        formEl.InnerHtml += " " + seachButton;
                                    }

                                    MenuContainer.InnerHtml += formEl;
                                }
                            }

                        }
                    }
                }
            }
            mainContainer.InnerHtml += header;
            mainContainer.InnerHtml += MenuContainer;
            nav.InnerHtml += mainContainer;

            return new MvcHtmlString(nav.ToString(TagRenderMode.Normal));
        }
    }
}