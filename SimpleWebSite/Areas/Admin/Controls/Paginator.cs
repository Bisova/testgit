﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleWebSite.Areas.Admin.Controls
{
    public static class Paginator
    {

        public static MvcHtmlString PaginatorControl(this HtmlHelper html, string Url, string PageParam, string PaginatorId, int CurrentPage, int PageSize, int TotalRows, string onClickFunction)
        {
            int PageCount = (TotalRows % PageSize > 0) ? (TotalRows / PageSize) + 1 : TotalRows / PageSize;

            TagBuilder Script = new TagBuilder("script");
            Script.Attributes.Add("type", "text/javascript");
            if (!string.IsNullOrEmpty(onClickFunction))
            {
                Script.InnerHtml +=
                    /*"var init = true, " +
                        "state = window.history.pushState !== undefined;" +
                    "$.address.state('/').change(function (event) {" +
                        "if (state && init) {" +
                            "init = false;" +
                        "} else {" +
                            onClickFunction + "(event.value);" +
                        "}" +
                     "});" +
                    "if (!state) {" +
                    "document.write('<style type=\"text/css\"> .page { display: none; } </style>');" +
                        "}" +*/
                    "$(document).ready(function(){" +
                        "$('#" + PaginatorId + ">li>a').on('click', function(){" +
                            "$.address.value($(this).attr('href'));" +
                        "});" +
                    "});";
            }

            TagBuilder UL = new TagBuilder("ul");
            UL.Attributes.Add("class", "pagination");
            UL.Attributes.Add("id", PaginatorId);
            UL.Attributes.Add("style", "text-decoration:none;");

            TagBuilder LIPrev = new TagBuilder("li");
            TagBuilder APrev = new TagBuilder("a");
            APrev.InnerHtml = "&laquo;";

            if (CurrentPage <= 1)
                LIPrev.Attributes.Add("class", "disabled");
            else
                APrev.Attributes.Add("href", ReWrightUrlParametr(Url, PageParam, (CurrentPage - 1).ToString()));

            LIPrev.InnerHtml += APrev;
                

            UL.InnerHtml += LIPrev;

            for (int i = 1; i <= PageCount; i++)
            {
                TagBuilder LIPage = new TagBuilder("li");
                TagBuilder APage = new TagBuilder("a");
                APage.Attributes.Add("href", ReWrightUrlParametr(Url, PageParam, i.ToString()));
                APage.InnerHtml = i.ToString();
                if (CurrentPage == i)
                    LIPage.Attributes.Add("class", "active");
                LIPage.InnerHtml += APage;
                UL.InnerHtml += LIPage;
            }

            TagBuilder LINext = new TagBuilder("li");
            TagBuilder ANext = new TagBuilder("a");
            ANext.InnerHtml = "&raquo;";

            if (CurrentPage >= PageCount)
                LINext.AddCssClass("disabled");
            else
                ANext.Attributes.Add("href", ReWrightUrlParametr(Url, PageParam, (CurrentPage + 1).ToString()));

            LINext.InnerHtml += ANext;
            

            UL.InnerHtml += LINext;

            return new MvcHtmlString(Script.ToString(TagRenderMode.Normal) + UL.ToString(TagRenderMode.Normal));
        }

        private static string ReWrightUrlParametr(string Url, string ParamName, string ParamValue)
        {
            string[] UrlParts = Url.Split('?');
            string result = UrlParts[0];

            if (UrlParts.Length > 1)
            {
                string resultPart2 = "";
                int counter = 0;
                string[] Params = UrlParts[1].Split('&');
                foreach (string s in Params)
                {
                    string[] paramParts = s.Split('=');
                    if (paramParts[0] == ParamName)
                    {
                        counter++;
                        paramParts[1] = ParamValue;
                    }

                    resultPart2 += (resultPart2 == "") ? "?" + paramParts[0] + "=" + paramParts[1] : "&" + paramParts[0] + "=" + paramParts[1];
                }

                if (counter == 0)
                    resultPart2 += (resultPart2 == "") ? "?" + ParamName + "=" + ParamValue : "&" + ParamName + "=" + ParamValue;

                result += resultPart2;
            }
            else
            {
                result += "?" + ParamName + "=" + ParamValue;
            }

            return result;
        }
    }
}