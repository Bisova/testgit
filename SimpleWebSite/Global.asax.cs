﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace SimpleWebSite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            CaptureControl.RouteConfig.RegisterRoutes(RouteTable.Routes);
            SimpleWebSiteLibrary.errors.InitLogger();
            SimpleWebSiteLibrary.errors.InfoMessageEx = "Application Start";
            
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Session.Timeout = SimpleWebSiteLibrary.SystemSettings.MinutesOnline;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Session.Timeout = SimpleWebSiteLibrary.SystemSettings.MinutesOnline;
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var context = new HttpContextWrapper(Context);
            if (context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
                Context.Response.RedirectLocation = null;
            else
            {
                if (User.Identity.IsAuthenticated)
                    SimpleWebSiteLibrary.User.UpdateUserActivity(SimpleWebSiteLibrary.User.GetUserIdByLogin(User.Identity.Name));
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext oHttpContext;
            Exception oException;
            RouteData routeData = new RouteData();

            oHttpContext = HttpContext.Current;

            oException = oHttpContext.Server.GetLastError();

            if (oException is HttpException)
            {
                switch ((oException as HttpException).GetHttpCode())
                {
                    case 404:

                        oHttpContext.Response.StatusCode = 404;
                        oHttpContext.Response.StatusDescription = "Not Found";
                        try
                        {
                            routeData.Values.Add("action", "Index");
                            routeData.Values.Add("controller", "ErrorPage");
                            routeData.Values.Add("message", oException.Message);
                        }
                        catch { }
                        oHttpContext.Server.ClearError();

                        IController errorController = new SimpleWebSite.Controllers.ErrorPageController();
                        errorController.Execute(new RequestContext(
                            new HttpContextWrapper(Context), routeData));

                        break;
                    default:

                        SimpleWebSiteLibrary.errors.error_messageEx = new Exception("Необработанное исключение: " + oException.GetType().Name +
                            Environment.NewLine + oException.Message +
                            Environment.NewLine + "STACK TRACE:" +
                            Environment.NewLine + oException.StackTrace);
                        break;
                }


            }
            else if (oException.HResult == -2146233079 && Request.Url.Host != "localhost")
            {
                oHttpContext.Response.StatusCode = 404;
                oHttpContext.Response.StatusDescription = "Not Found";
                try
                {
                    routeData.Values.Add("action", "Index");
                    routeData.Values.Add("controller", "ErrorPage");
                    routeData.Values.Add("message", oException.Message);
                }
                catch { }
                oHttpContext.Server.ClearError();

                IController errorController = new SimpleWebSite.Controllers.ErrorPageController();
                errorController.Execute(new RequestContext(
                    new HttpContextWrapper(Context), routeData));
            }
            else
            {
                SimpleWebSiteLibrary.sql s = new SimpleWebSiteLibrary.sql();
                if (!s.CheckConnection())
                {
                    try
                    {
                        routeData.Values.Add("action", "Unavaliable");
                        routeData.Values.Add("controller", "ErrorPage");
                        routeData.Values.Add("message", oException.Message);
                    }
                    catch { }
                    oHttpContext.Server.ClearError();

                    IController sqlController = new SimpleWebSite.Controllers.ErrorPageController();
                    sqlController.Execute(new RequestContext(
                        new HttpContextWrapper(Context), routeData));
                }
                else
                {
                    SimpleWebSiteLibrary.errors.error_messageEx = oException;
                }
            }
        }

    }
}