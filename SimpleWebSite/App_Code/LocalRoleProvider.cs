﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration.Provider;
using SimpleWebSiteLibrary;

public class LocalRoleProvider : RoleProvider
{
    /// <summary>
    /// Инициализация класса поставщика членства
    /// </summary>
    /// <param name="name">Наименование поставщика</param>
    /// <param name="config">Список свойств конфигурации</param>
    public override void Initialize(string name, NameValueCollection config)
    {
        _Role = new UserRoles();
        _RoleRow = new UserRolesRow();
        _RoleCollection = new UserRolesCollection();
        _RoleRowCollection = new UserRolesRowCollection();
        if (config == null)
            throw new ArgumentNullException("config");

        if (name == null || name.Length == 0)
            name = "CustomMembershipProvider";
        base.Initialize(name, config);

        _applicationName = GetConfigValue(config["applicationName"],
            System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
    }
    #region<<Переменные>>
    private string _applicationName;
    private UserRoles _Role;
    private UserRolesRow _RoleRow;
    private UserRolesCollection _RoleCollection;
    private UserRolesRowCollection _RoleRowCollection;
    #endregion

    #region<<Свойства>>
    /// <summary>
    /// Наименование приложения
    /// </summary>
    public override string ApplicationName
    {
        get
        {
            return _applicationName;
        }
        set
        {
            _applicationName = value;
        }
    }



    #endregion

    #region<<Методы>>
    /// <summary>
    /// Вспомогательный метод для извлечения параметров провайдера из конфигурационного файла Web.config
    /// </summary>
    /// <param name="configValue">Значение из конфигурационного файла</param>
    /// <param name="defaultValue">Значение по-умолчанию, в случае если в конфигурации оно не установлено</param>
    /// <returns>Возвращает значение параметра провайдера</returns>
    private string GetConfigValue(string configValue, string defaultValue)
    {
        return (string.IsNullOrEmpty(configValue)) ? defaultValue : configValue;
    }

    /// <summary>
    /// Добавление заданных пользователей в заданные группы
    /// </summary>
    /// <param name="usernames">Массив со списком пользоваталей</param>
    /// <param name="roleNames">Массив со списком наименований ролей</param>
    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
        foreach (string rolename in roleNames)
        {
            if (rolename == null || rolename == "")
                throw new ProviderException("Имя роли не может быть пустым значением или null");
            if (!UserRoles.Exists(rolename))
                throw new ProviderException("Роль с указанным именем не найдена");
        }

        foreach (string username in usernames)
        {
            if (username == null || username == "")
                throw new ProviderException("Имя пользователя не может быть пусты значение или null");
            if (username.Contains(","))
                throw new ArgumentException("Имя пользователя не может содержать запятые");

            foreach (string rolename in roleNames)
            {
                if (UserRoles.IsUserInRole(username, rolename))
                    throw new ProviderException("Пользователь " + username + " уже принадлежит роли " + rolename);
            }
        }

        int user_add = User.GetUserIdByLogin(HttpContext.Current.User.Identity.Name);


        foreach (string username in usernames)
        {
            foreach (string rolename in roleNames)
            {
                _RoleRow.Add(username, rolename, user_add);
            }
        }
    }

    /// <summary>
    /// Создание новой роли пользователя
    /// </summary>
    /// <param name="roleName">Наименование роли пользователя</param>
    public override void CreateRole(string roleName)
    {
        string user_login = HttpContext.Current.User.Identity.Name;
        int user_id = User.GetUserIdByLogin(user_login);
        _Role.Create(roleName, string.Empty, user_id);
    }
    /// <summary>
    /// Удаление роли пользователя из системы
    /// </summary>
    /// <param name="roleName">Наименование роли пользователя в системе</param>
    /// <param name="throwOnPopulatedRole">Выдавать ли исключение, если для заданной роли уже были заданы пользователи</param>
    /// <returns>Возвращает true, если роль была успешно удалена, в противномл случае - false</returns>
    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
        if (!UserRoles.Exists(roleName))
            throw new System.Configuration.Provider.ProviderException("Роль с указанным именем не существует в ситеме");

        if (throwOnPopulatedRole && UserRoles.HasUsers(roleName))
            throw new System.Configuration.Provider.ProviderException("Невозможно удалить роль, т.к. для заданной роли уже были назначены пользователи");

        return _Role.Delete(roleName);
    }
    /// <summary>
    /// Найти пользователей в заданной роли
    /// </summary>
    /// <param name="roleName">Наименование роли</param>
    /// <param name="usernameToMatch">Наименование пользователя</param>
    /// <returns>Возвращает массив со списком пользователей</returns>
    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
        UserCollection _Users = new UserCollection();
        DataTable dt = _Users.FindUsersInRole(roleName, usernameToMatch);
        if (dt != null)
        {
            string[] result = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                result[i] = dt.Rows[i]["sAMAccount"].ToString();
            }
            return result;
        }
        else
        {
            throw new ProviderException("Ошибка при извлечении данных");
        }
    }
    /// <summary>
    /// Получение всех ролей в системе
    /// </summary>
    /// <returns>Возвращает массив со списком всех ролей в системе</returns>
    public override string[] GetAllRoles()
    {
        DataTable dt = _RoleCollection.GetAllRoles();
        if (dt != null)
        {
            string[] result = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                result[i] = dt.Rows[i]["name"].ToString();
            }
            return result;
        }
        else
        {
            throw new ProviderException("Ошибка при извлечении данных");
        }
    }
    /// <summary>
    /// Получение списка ролей для заданного пользователя
    /// </summary>
    /// <param name="username">Имя пользователя (логин) в системе</param>
    /// <returns>Возвращает массив со списком ролей для заданного пользователя</returns>
    public override string[] GetRolesForUser(string username)
    {
        DataTable dt = _RoleCollection.GetUserRoles(username);

        if (dt != null)
        {
            string[] result = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                result[i] = dt.Rows[i]["name"].ToString();
            }
            return result;
        }
        else
        {
            return null;
        }

    }
    /// <summary>
    /// Получение списка пользователей для заданной роли
    /// </summary>
    /// <param name="roleName">Наименование роли</param>
    /// <returns>Возвращает массив со списком пользователей для заданного роли</returns>
    public override string[] GetUsersInRole(string roleName)
    {
        UserCollection _Users = new UserCollection();
        DataTable dt = _Users.GetRoleUsers(roleName);
        if (dt != null)
        {
            string[] result = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                result[i] = dt.Rows[i]["sAMAccount"].ToString();
            }
            return result;
        }
        else
        {
            throw new ProviderException("Ошибка при извлечении данных");
        }
    }
    /// <summary>
    /// Принадлежит ли пользователь с заданным имененм роли с заданным именем
    /// </summary>
    /// <param name="username">Имя пользователя (логин) в системе</param>
    /// <param name="roleName">Имя роли в системе</param>
    /// <returns>Возвращает true, если пользователь существует для заданной роли, в противном случае - false</returns>
    public override bool IsUserInRole(string username, string roleName)
    {
        return UserRoles.IsUserInRole(username, roleName);
    }
    /// <summary>
    /// Удаление пользователей из указанных ролей
    /// </summary>
    /// <param name="usernames">Массив с именами пользователей в системе</param>
    /// <param name="roleNames">Массив с именами ролей, для которых удаляем пользователя</param>
    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
        foreach (string rolename in roleNames)
        {
            if (rolename == null || rolename == "")
                throw new ProviderException("Имя роли не может быть пустым значением или null");
            if (!UserRoles.Exists(rolename))
                throw new ProviderException("Роль с указанным именем не найдена");
        }

        foreach (string username in usernames)
        {
            if (username == null || username == "")
                throw new ProviderException("Имя пользователя не может быть пусты значение или null");
            if (username.Contains(","))
                throw new ArgumentException("Имя пользователя не может содержать запятые");

            foreach (string rolename in roleNames)
            {
                if (!UserRoles.IsUserInRole(username, rolename))
                    throw new ProviderException("Пользователь " + username + " не принадлежит роли " + rolename);
            }
        }

        foreach (string username in usernames)
        {
            foreach (string rolename in roleNames)
            {
                _RoleRow.Delete(username, rolename);
            }
        }
    }
    /// <summary>
    /// Существует ли роль с заданным именем в системе
    /// </summary>
    /// <param name="roleName">Имя роли в системе</param>
    /// <returns>Возвращает true, если роль с указанным именем существует в системе, в противном случае - false</returns>
    public override bool RoleExists(string roleName)
    {
        return UserRoles.Exists(roleName);
    }

    #endregion

}
