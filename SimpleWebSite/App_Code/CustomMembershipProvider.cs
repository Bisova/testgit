﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Security;
using SimpleWebSiteLibrary;

public class LocalMembershipProvider : MembershipProvider
{

    /// <summary>
    /// Инициализация класса поставщика членства
    /// </summary>
    /// <param name="name">Наименование поставщика</param>
    /// <param name="config">Список свойств конфигурации</param>
    public override void Initialize(string name, NameValueCollection config)
    {
        _user = new User();
        _userCollection = new UserCollection();

        if (config == null)
            throw new ArgumentNullException("config");

        if (name == null || name.Length == 0)
            name = "CustomMembershipProvider";

        if (String.IsNullOrEmpty(config["description"]))
        {
            config.Remove("description");
            config.Add("description", "Custom Membership Provider");
        }

        base.Initialize(name, config);

        _ApplicationName = GetConfigValue(config["applicationName"],
            System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

        _MaxInvalidPasswordAttempts = Convert.ToInt32(
            GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));

        _PasswordAttemptWindow = Convert.ToInt32(
            GetConfigValue(config["passwordAttemptWindow"], "10"));

        _MinRequiredNonAlphanumericCharacters = Convert.ToInt32(
            GetConfigValue(config["minRequiredNonalphanumericCharacters"], "1"));

        _MinRequiredPasswordLength = Convert.ToInt32(
            GetConfigValue(config["minRequiredPasswordLength"], "6"));

        _EnablePasswordReset = Convert.ToBoolean(
            GetConfigValue(config["enablePasswordReset"], "true"));

        _PasswordStrengthRegularExpression = Convert.ToString(
            GetConfigValue(config["passwordStrengthRegularExpression"], ""));

        _EnablePasswordRetrieval = Convert.ToBoolean(
            GetConfigValue(config["enablePasswordRetrieval"], "false"));
        if (GetConfigValue(config["passwordFormat"], "Hashed") == "Hashed")
            _PasswordFormat = MembershipPasswordFormat.Hashed;
        else if (GetConfigValue(config["passwordFormat"], "Hashed") == "Encrypted")
            _PasswordFormat = MembershipPasswordFormat.Encrypted;
        else
            _PasswordFormat = MembershipPasswordFormat.Clear;
    }


    #region<<Переменные>>

    private User _user;
    private UserCollection _userCollection;
    private string _ApplicationName;
    private bool _EnablePasswordReset;
    private bool _EnablePasswordRetrieval;
    private int _MaxInvalidPasswordAttempts;
    private int _MinRequiredNonAlphanumericCharacters;
    private int _MinRequiredPasswordLength;
    private int _PasswordAttemptWindow;
    private MembershipPasswordFormat _PasswordFormat;
    private string _PasswordStrengthRegularExpression;

    #endregion

    #region<<Свойства>>

    public override string ApplicationName
    {
        get
        {
            return _ApplicationName;
        }
        set
        {
            _ApplicationName = value;
        }
    }
        
    public override bool EnablePasswordReset
    {
        get { return _EnablePasswordReset; }
    }
        
    public override bool EnablePasswordRetrieval
    {
        get { return _EnablePasswordRetrieval; }
    }

    public override int MaxInvalidPasswordAttempts
    {
        get { return _MaxInvalidPasswordAttempts; }
    }

    public override int MinRequiredNonAlphanumericCharacters
    {
        get { return _MinRequiredNonAlphanumericCharacters; }
    }

    public override int MinRequiredPasswordLength
    {
        get { return _MinRequiredPasswordLength; }
    }

    public override int PasswordAttemptWindow
    {
        get { return _PasswordAttemptWindow; }
    }

    public override MembershipPasswordFormat PasswordFormat
    {
        get { return _PasswordFormat; }
    }

    public override bool RequiresQuestionAndAnswer
    {
        get { return false; }
    }

    public override bool RequiresUniqueEmail
    {
        get { return true; }
    }

    /// <summary>
    ///  Регулярное выражение, используемое для оценки пароля
    /// </summary>
    public override string PasswordStrengthRegularExpression
    {
        get { return ""; }
    }



    #endregion

    #region<<Методы>>

    /// <summary>
    /// Вспомогательный метод для извлечения параметров провайдера из конфигурационного файла Web.config
    /// </summary>
    /// <param name="configValue">Значение из конфигурационного файла</param>
    /// <param name="defaultValue">Значение по-умолчанию, в случае если в конфигурации оно не установлено</param>
    /// <returns>Возвращает значение параметра провайдера</returns>
    private string GetConfigValue(string configValue, string defaultValue)
    {
        return (string.IsNullOrEmpty(configValue)) ? defaultValue : configValue;
    }
    /// <summary>
    /// Изменение пароля пользователя в системе
    /// </summary>
    /// <param name="username">Логин пользователя в системе</param>
    /// <param name="oldPassword">Текущий пароль пользователя</param>
    /// <param name="newPassword">Новый пароль пользователя</param>
    /// <returns>Возвращает true, если проль был успешно изменен, в противном случа - false</returns>
    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
        int resultKey;
        string resultMess;
        return _user.ChangePassword(username, oldPassword, newPassword, out resultKey, out resultMess);
    }

    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
        return false;
    }

    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
        throw new NotImplementedException();
    }

    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
        throw new NotImplementedException();
    }

    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new NotImplementedException();
    }

    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new NotImplementedException();
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
        throw new NotImplementedException();
    }

    public override int GetNumberOfUsersOnline()
    {
        throw new NotImplementedException();
    }

    public override string GetPassword(string username, string answer)
    {
        throw new NotImplementedException();
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
        throw new NotImplementedException();
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
        throw new NotImplementedException();
    }

    public override string GetUserNameByEmail(string email)
    {
        throw new NotImplementedException();
    }

    public override string ResetPassword(string username, string answer)
    {
        throw new NotImplementedException();
    }

    public override bool UnlockUser(string userName)
    {
        throw new NotImplementedException();
    }

    public override void UpdateUser(MembershipUser user)
    {
        
        throw new NotImplementedException();
    }

    public override bool ValidateUser(string username, string password)
    {
        string message = string.Empty;
        return _user.ValidateUser(username, password, out message);
    }

    #endregion







}
