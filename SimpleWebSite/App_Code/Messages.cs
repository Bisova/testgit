﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
using SimpleWebSiteLibrary;

namespace SimpleWebSite.App_Code
{
    public static class Messages
    {
        public static MvcHtmlString Message(this HtmlHelper html, string messageKey)
        {
            return new MvcHtmlString(sql.Message(messageKey));
        }
    }
}