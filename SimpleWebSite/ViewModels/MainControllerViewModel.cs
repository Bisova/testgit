﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimpleWebSiteLibrary;

namespace SimpleWebSite.ViewModels
{
    public class MainControllerViewModel
    {
        /// <summary>
        /// Класс модели главного контроллера приложения
        /// </summary>
        public MainControllerViewModel()
        {
            
        }

        #region<<Переменные>>

        private About _aboutInfo;

        #endregion

        #region<<Свойства>>

        /// <summary>
        /// Информаци об организации
        /// </summary>
        public About AboutInfo
        {
            get
            {
                if (_aboutInfo == null)
                {
                    _aboutInfo = new About();
                    _aboutInfo.Fill();
                }
                return _aboutInfo;
            }
        }
        /// <summary>
        /// Текущий раздел сайта
        /// </summary>
        public string CurrentMenu
        {
            get
            {
                return HttpContext.Current.Request.RawUrl.Split('/')[1];
            }
        }
        /// <summary>
        /// Контейнер для передачи дочернего объекта в представление
        /// </summary>
        public object ChildViewModel { get; set; }
        /// <summary>
        /// Текущий год
        /// </summary>
        public int CurrentYear
        {
            get
            {
                return DateTime.Now.Year;
            }
        }

        /// <summary>
        /// Подтвердил ли пользователь свой аккаунт в системе
        /// </summary>
        public bool IsUserConfirmed
        {
            get
            {
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    return false;
                bool result;
                if((System.Web.HttpContext.Current.Session["userconfirmed"] == null))
                    return SimpleWebSiteLibrary.User.CheckConfirmation(System.Web.HttpContext.Current.User.Identity.Name);
                if (Convert.ToBoolean(System.Web.HttpContext.Current.Session["userconfirmed"].ToString()))
                    result = true;
                else
                    return SimpleWebSiteLibrary.User.CheckConfirmation(System.Web.HttpContext.Current.User.Identity.Name);
                return result;
            }
        }
        /// <summary>
        /// ФИО текущего пользователя
        /// </summary>
        public string CurrentUserFIO
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.Session["UserFIO"] != null)
                    return (string.IsNullOrEmpty(HttpContext.Current.Session["UserFIO"].ToString()))?
                        HttpContext.Current.User.Identity.Name:
                        HttpContext.Current.Session["UserFIO"].ToString();
                else
                    return string.Empty;
            }
        }


        #endregion

        #region<<Методы>>

        #endregion
    }
}